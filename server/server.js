var express = require('express');
var sql = require("mssql");

var dbConfig = {
    server: "localhost",
    database: "Image",
    user: "sa",
    password: "Test123",
    port: 1433
};


// define server
var app = express();
/* app configuration */
var bodyParser = require('body-parser');
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

var conn = new sql.Connection(dbConfig);
conn.connect();
var request = new sql.Request(conn);


var createTable = function (response) {
    var createquery='CREATE TABLE [ImageStore]([id] [int] IDENTITY(1,1) NOT NULL ,[name] [varchar](50) NULL,[image] [varchar](max) NULL,CONSTRAINT [PK_ImageStore] PRIMARY KEY CLUSTERED ([id] ASC))'
    request.query(createquery, function (err, recordSet) {
        if (err) {
            console.log(err)
        }
        else {
            response.send('200', recordSet);
        }
    });
}
app.get('/ImageStore', function (req, response) {

    request.query("select * from ImageStore", function (err, recordSet) {
        if (err) {
            response.send('400', err);
            createTable(response);
        }
        else {
            response.send(recordSet);
        }

    });

});
app.post('/ImageStore', function (req, response) {
    var name = req.body.name;
    var image = req.body.image
    var query = "insert into ImageStore values('" + name + "','" + image + "')";
    request.query(query, function (err, recordSet) {
        if (err) {
            response.send('400', err);
        }
        else {

            response.send(recordSet);
        }

    });


});
app.delete('/ImageStore/:id', function (req, response) {
    console.log('', req.params.id);
    var query = "DELETE FROM ImageStore WHERE id=" + req.params.id;
    request.query(query, function (err, recordSet) {
        if (err) {
            response.send('400', err);
        }
        else {

            response.send(recordSet);
        }

    });


});
app.listen(3000);