﻿//Define an angular module for our app
var sampleApp = angular.module('sampleApp', []);
var api = 'http://localhost:3000/';
//Define Routing for app
//Uri /AddNewOrder -> template AddOrder.html and Controller AddOrderController
//Uri /ShowOrders -> template ShowOrders.html and Controller AddOrderController
sampleApp.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.when('/admin', {
          templateUrl: 'module/view/admin.html',
          controller: 'AdminCtrl'
      });
      $routeProvider.when('/user', {
          templateUrl: 'module/view/user.html',
          controller: 'UserCtrl'
      }).otherwise({
          redirectTo: '/user'
      });
  }]);


sampleApp.controller('AdminCtrl', function ($scope, $http, $location) {

    $scope.message = 'This is Add new order screen';
    $http.get(api + 'ImageStore').success(function (res) {
        //   $scope.results = res;

    }).error(function (res) {
        var errorMessage = (res.error) ? 'email id already exist' : ' unable to create user';
    });

    $scope.upload = function (input) {
        var image = {
            name: $scope.inputFile.file.name,
            image: $scope.inputFile.dataURL
        }
        $http.post(api + 'ImageStore', image).success(function (res) {
            $scope.inputFile = undefined;

        }).error(function (res) {
        });
    }
    $scope.goToUser = function () {
        $location.path('/user');
    }
});
sampleApp.controller('UserCtrl', function ($scope, $http, $location) {
    $scope.show = false;
    $scope.getImage = function () {
        $http.get(api + 'ImageStore').success(function (res) {
            $scope.results = res;
        }).error(function (res) {
        });
    }
    $scope.getImage();
    $scope.goToAdmin = function () {
        $location.path('/admin');
    }
    $scope.editImage = function (result) {

    }
    $scope.deleteImage = function (result) {
        $http.delete(api + 'ImageStore/' + result.id).success(function (res) {
            debugger;
            $scope.getImage();
        }).error(function (res) {
        });

    }
});
//$('div span').mouseenter(function () {

//    $(this).children('p').show();

//});

//$('ul li').mouseleave(function () {

//    $(this).children('p').hide();

//});
sampleApp.directive('file', function ($q) {

    var fileToDataURL = function (file) {
        var deferred = $q.defer();
        var reader = new FileReader();
        reader.onload = function (e) {

            deferred.resolve(e.target.result);

        };
        reader.readAsDataURL(file);
        return deferred.promise;
    };

    return {
        restrict: 'E',
        template: '<input type="file" />',
        replace: true,
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            var listener = function (value) {
                scope.$apply(function () {
                    attr.multiple ? ctrl.$setViewValue(element[0].files) : ctrl.$setViewValue(element[0].files[0]);
                    ctrl.$setViewValue(value)
                });
            }
            element.bind('change', function (evt) {
                scope.inputFile = [];
                var files = evt.target.files;
                var imageResult = {
                    file: files[0],
                    url: URL.createObjectURL(files[0])
                };
                var reader = new FileReader();
                reader.onload = function (e) {
                    imageResult.dataURL = (e.target.result);
                    listener(imageResult);
                };
                reader.readAsDataURL(files[0]);
            })
        }
    }
});